package ua.spellchecker;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import ua.algorithms.Pair;
import ua.parser.Reader;

public class IsolatedTermCorrectionTest extends TestCase {

	private final static String BIGTEXT = "./resources/fullWordList_pruned_at_20.txt";
	private final static String TESTWORDLIST = "./resources/Commonly_misspelled_words.txt";
	
	private static Map<String, Integer> wordCount;
	private static Suggestor s1;
	private static Suggestor s2;
	private static ArrayList<Pair<String,String>> wordpairs;
	
	public IsolatedTermCorrectionTest(String name) {
		super(name);
	}

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( IsolatedTermCorrectionTest.class );
    }
	
	@Override
	protected void setUp() {
        try {
			
			this.wordCount = Reader.getCounts(BIGTEXT);

			this.s1 = new SlowLevenshteinSuggestor(wordCount);
			this.s2 = new LevenshteinSuggestor(wordCount);
			
			
			this.wordpairs = new ArrayList<Pair<String, String>>();
			try {
				BufferedReader br = new BufferedReader(new FileReader(TESTWORDLIST));
			    String line;
			    while ((line = br.readLine()) != null) {
			       String[] pair = line.split("->");
			       if (pair.length == 2) {
			    	   this.wordpairs.add(new Pair<String, String>(pair[0], pair[1]));
			       }
			    }
			} finally {
			}
			

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/*
	public void testlevenshteinDFA() {
		assertTrue(s2.getBestSuggestion("alright", this.tc).equalsIgnoreCase("alright"));
		assertTrue(s2.getBestSuggestion("alrigt", this.tc).equalsIgnoreCase("alright"));
		assertTrue(s2.getBestSuggestion("collaboratias", this.tc).equalsIgnoreCase("collaboration"));
		assertTrue(s2.getBestSuggestion("pratnership", this.tc).equalsIgnoreCase("partnership"));
		assertTrue(s2.getBestSuggestion("wor", this.tc).equalsIgnoreCase("for"));
		assertTrue(s2.getBestSuggestion("teht", this.tc).equalsIgnoreCase("test"));
		assertTrue(s2.getBestSuggestion("houde", this.tc).equalsIgnoreCase("house"));
	}
	*/
	
	public void testSlowImpl() {
		double score = 0;
		long start = System.currentTimeMillis();
		int falsepositives = 0;
		for (Pair<String,String> pair : wordpairs) {
			if (wordCount.containsKey(pair.one())) {
				falsepositives +=1;
				continue;
			}
			String sugg = s1.getBestSuggestion(pair.one());
			//System.out.println(sugg);
			if (sugg.equalsIgnoreCase(pair.two())) {
				score += 1;
			}
		}
		
		long end = System.currentTimeMillis();
		int wps = (int) Math.floor(1000*wordpairs.size()/(end - start));
		System.out.println("Using levenshtein distance\n==================================================");
		System.out.println("Average correcting speed: " + wps + " wps");
		System.out.println("Total time: " + (end - start) + " ms");
		score = score/(wordpairs.size()-falsepositives);
		System.out.println("Score: " + score);
		System.out.println("==================================================");
		assertTrue(score > 0.6);
	}
	
	public void testAll() {
		double score = 0;
		long start = System.currentTimeMillis();
		int falsepositives = 0;
		for (Pair<String,String> pair : wordpairs) {
			if (wordCount.containsKey(pair.one())) {
				falsepositives +=1;
				continue;
			}
				
			String sugg = s2.getBestSuggestion(pair.one());
			//System.out.println(sugg);
			if (sugg.equalsIgnoreCase(pair.two())) {
				score += 1;
			}
		}
		
		long end = System.currentTimeMillis();
		int wps = (int) Math.floor(1000*wordpairs.size()/(end - start));
		System.out.println("Using levenshtein automata\n==================================================");
		System.out.println("Average correcting speed: " + wps + " wps");
		System.out.println("Total time: " + (end - start) + " ms");
		score = score/(wordpairs.size()-falsepositives);
		System.out.println("Score: " + score);
		System.out.println("==================================================");

		assertTrue(score > 0.6);
	}
	

}

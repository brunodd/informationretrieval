package ua.spellchecker;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import ua.algorithms.Pair;
import ua.parser.Reader;

public class ContextSensitiveCorrectionTest extends TestCase {

	
	private static String WORDLIST = "./resources/fullWordList_pruned_at_20.txt";
	private static String TRIGRAMLIST1 = "./resources/bnc_trigrams.txt";
	private static String TESTPHRASELIST = "./resources/phrases.txt";
	
	private Suggestor suggestor;
	private Map<String, String> dictionary;

    
	@Override
	protected void setUp() {
		this.suggestor = new SpellSuggestor(Reader.getTrigramCounts(TRIGRAMLIST1),
					Reader.getCounts(WORDLIST));
		this.dictionary = new HashMap<String, String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(TESTPHRASELIST));
		    String line;
		    while ((line = br.readLine()) != null) {
		       String[] pair = line.split("->");
		       if (pair.length == 2) {
		    	   this.dictionary.put(pair[0], pair[1]);
		       }
		    }
		    br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
    public static Test suite()
    {
        return new TestSuite( ContextSensitiveCorrectionTest.class );
    }
    
    private String correct(String s1) {
    	return this.suggestor.getBestSuggestion(s1);
    }

    public void testPhrases() {
		double score = 0;
		long start = System.currentTimeMillis();

		for (String sentence : this.dictionary.keySet()) {
			String corrected = this.correct(sentence);

			if (corrected == null) {
	    		corrected = sentence;
			}

			if (corrected.equalsIgnoreCase(this.dictionary.get(sentence))) {
				score += 1;
				System.out.println("[SUCCESS]");
			} else {
				System.out.println("[FAIL]");
			}
			System.out.println("\t'" + sentence + "' :\n\t'" + corrected + "'.");
	    	
		}
		
		long end = System.currentTimeMillis();
		int wps = (int) Math.floor(1000*this.dictionary.size()/(end - start));
		System.out.println("Average correcting speed: " + wps + " phrases/s");
		System.out.println("Total time: " + (end - start) + " ms");
		score = score/this.dictionary.size();
		System.out.println("Score: " + score);
		
		assertTrue(score >= 0.6);
    }
    
}

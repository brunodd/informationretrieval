package ua.spellchecker;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import ua.algorithms.Pair;
import ua.parser.Reader;

public class TrigramCorrectionTest extends TestCase {

	private static TriGramSuggestor sugg;
	private static String TRIGRAMLIST = "./resources/bnc_trigrams.txt";
	private static String WORDCOUNT = "./resources/fullWordList_pruned_at_20.txt";
	private static LevenshteinSuggestor s2;
	private Map<String, String> dictionary;

    
	@Override
	protected void setUp() {
    	try {
    		// If trigramsfile does not exist, generate a new one
    		File f = new File(TRIGRAMLIST);
    		if (!f.exists()) {
    			Reader.createTrigramFile("./resources/bnc.txt", TRIGRAMLIST);
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		this.dictionary = new HashMap<String, String>();
		this.dictionary.put("is to high", "is too high");
		this.dictionary.put("to high in", "too high in");
		this.dictionary.put("high in the", "high in the");
		this.dictionary.put("in the ska", "in the sky");
		
		this.dictionary.put("get id done", "get it done");
		

    	sugg = new TriGramSuggestor(Reader.getTrigramCounts(TRIGRAMLIST), Reader.getCounts(WORDCOUNT));
    	
	}
	
    public static Test suite()
    {
        return new TestSuite( TrigramCorrectionTest.class );
    }
    
    public void testTrigrams() {
		double score = 0;
		long start = System.currentTimeMillis();

		for (String sentence : this.dictionary.keySet()) {
			String corrected = sugg.getBestSuggestion(sentence);
			
	    	if (corrected != null) {
	    		System.out.println("Best match for:\n\t'" + sentence + "' : \n\t'" + corrected + "'.");
	    	} else {
	    		System.out.println("No correction found for: '" + sentence + "'.");
	    		corrected = sentence;
	    	}
	    	
			if (corrected.equalsIgnoreCase(this.dictionary.get(sentence))) {
				score += 1;
				System.out.println("trigram is correct");
			}
			
		}
		
		long end = System.currentTimeMillis();
		int wps = (int) Math.floor(1000*this.dictionary.size()/(end - start));
		System.out.println("Average correcting speed: " + wps + " wps");
		System.out.println("Total time: " + (end - start) + " ms");
		score = score/this.dictionary.size();
		System.out.println("Score: " + score);
		
		assertTrue(score >= 0.6);
    }
}

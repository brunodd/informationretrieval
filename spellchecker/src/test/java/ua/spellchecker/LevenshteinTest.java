package ua.spellchecker;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import ua.algorithms.LevenshteinAutomaton;

public class LevenshteinTest extends TestCase {

	LevenshteinAutomaton auto1,auto2;

	public LevenshteinTest(String name) {
		super(name);
	}

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( LevenshteinTest.class );
    }
    
    protected void setUp(){
        auto1 = new LevenshteinAutomaton("longword", 2);
        auto2 = new LevenshteinAutomaton("and", 1);
     }
    
    public void test1() {
    	auto1.printAutomaton("test_longword.dot");
    	assertTrue(auto1.matches("longword"));
    	assertTrue(auto1.matches("longwor"));
    	assertTrue(auto1.matches("longwo"));
    	assertTrue(auto1.matches("longrd"));
    	assertTrue(auto1.matches("lonword"));
    	assertTrue(auto1.matches("lgword"));
    	assertTrue(auto1.matches("ongword"));
    	assertTrue(auto1.matches("ngword"));
    	assertTrue(auto1.matches("langword"));
    	assertTrue(auto1.matches("longitrd"));
    	assertTrue(auto1.matches("longwordes"));
    	assertTrue(auto1.matches("longwords"));
    	assertTrue(auto1.matches("longwoe"));

    	
    	assertFalse(auto1.matches("loongwoords"));
    	assertFalse(auto1.matches("longwasp"));
    	assertFalse(auto1.matches("ngward"));
    	assertFalse(auto1.matches("lingwarda"));
    	assertFalse(auto1.matches("lloonngword"));
    	assertFalse(auto1.matches(""));
    	assertFalse(auto1.matches("lpngwordes"));
    	assertFalse(auto1.matches("lngwr"));
    }
    
    public void test2() {
    	assertTrue(auto2.matches("and"));
    	assertTrue(auto2.matches("an"));
    	assertTrue(auto2.matches("nd"));
    	assertTrue(auto2.matches("ant"));
    	assertTrue(auto2.matches("end"));
    	assertTrue(auto2.matches("ann"));
    	assertTrue(auto2.matches("ind"));
    	assertTrue(auto2.matches("andt"));
    	
    	assertFalse(auto2.matches("ends"));
    	assertFalse(auto2.matches("ants"));
    	assertFalse(auto2.matches("anit"));
    	assertFalse(auto2.matches("andes"));
    	assertFalse(auto2.matches("a"));
    	assertFalse(auto2.matches("n"));
    	assertFalse(auto2.matches("d"));

    	
    }
}

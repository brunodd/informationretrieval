package ua.spellchecker;

import java.lang.reflect.Array;
import java.util.Arrays;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import ua.utility.Util;

public class UtilTest extends TestCase {
	
	public UtilTest(String name) {
		super(name);
	}

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( UtilTest.class );
    }
    
    protected void setUp() {
    }
    
    public void testStripString() {
    	assertTrue(Arrays.equals(
    			new String[]{"he", "s"}, Util.stripString("he's")));
    	assertTrue(Arrays.equals(
    			new String[]{"hi"}, Util.stripString("hi")));
    	assertTrue(Arrays.equals(
    			new String[]{"hi"}, Util.stripString("Hi")));
    	assertTrue(Arrays.equals(
    			new String[]{"hello", "there"}, Util.stripString("hello, there")));
    	assertTrue(Arrays.equals(
    			new String[]{"hi", "he", "s", "dead"}, Util.stripString("Hi, he's DeAd?")));
    	assertTrue(Arrays.equals(
    			null, Util.stripString(" ")));
    	assertTrue(Arrays.equals(
    			null, Util.stripString("")));
    }
    
}

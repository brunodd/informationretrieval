package ua.spellchecker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import ua.parser.Reader;

public class TrigramSuggestorTest extends TestCase {

	private static TriGramSuggestor sugg;
	private static String TRIGRAMLIST = "./resources/bnc_trigrams_pruned_at_10.txt";
	private static String WORDCOUNT = "./resources/fullWordList_pruned_at_20.txt";
	public TrigramSuggestorTest(String name) {
		super(name);
	}

	public TrigramSuggestorTest() {
		super();
	}
	
    public static Test suite()
    {
        return new TestSuite( TrigramSuggestorTest.class );
    }

    @Override
    protected void setUp() {
    	try {
    		// If trigramsfile does not exist, generate a new one
    		File f = new File(TRIGRAMLIST);
    		if (!f.exists()) {
    			Reader.createTrigramFile("./resources/bnc.txt", TRIGRAMLIST);
    		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	List<String> trigramLists = new ArrayList<String>();
    	trigramLists.add(TRIGRAMLIST);
    	try {
			sugg = new TriGramSuggestor(Reader.getTrigramCounts(Reader.merge(trigramLists)), Reader.getCounts(WORDCOUNT));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testSuggestor()
    {
    	System.out.println(sugg.analyseSentence("I brake the law!"));
    	System.out.println(sugg.analyseSentence("I need a brake"));
    	System.out.println(sugg.analyseSentence("I want too break free"));
    	System.out.println(sugg.analyseSentence("The bird flee to high in the ska."));
    	System.out.println(sugg.analyseSentence("Please, fill in the from, sir."));
    	System.out.println(sugg.analyseSentence("This is grate!"));

        assertTrue( true );
    }
}

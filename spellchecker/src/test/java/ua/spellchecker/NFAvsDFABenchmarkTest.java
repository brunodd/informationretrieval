package ua.spellchecker;

import java.util.Map;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import ua.algorithms.LevenshteinAutomaton;
import ua.parser.Reader;

public class NFAvsDFABenchmarkTest extends TestCase {

	private LevenshteinSuggestor sugg;
	
    public static Test suite()
    {
        return new TestSuite( NFAvsDFABenchmarkTest.class );
    }
    
	@Override
	protected void setUp() {
		try {
			Map<String, Integer> wordCount = Reader.getCounts("./resources/wordsList.txt");
			this.sugg = new LevenshteinSuggestor(wordCount);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void testBenchmark() {
		printSuggestion("conteines");
		printSuggestion("aple");
		printSuggestion("patneshi");
		printSuggestion("terrists");
		printSuggestion("acheivements");
		printSuggestion("preceed");
		printSuggestion("ratitively");
		printSuggestion("persuedin");
	}
	
	public void printSuggestion(String word) {
		long start1 = System.currentTimeMillis();
		String suggestion1 = sugg.getBestSuggestionWithNFA(word);
		if (suggestion1 != null) {
			System.out.println("Best match for " + word + " = " + suggestion1);
		} else {
			System.out.println("No suggestion found for " + word);
		}
		long end1 = System.currentTimeMillis();
		System.out.println("Benchmark for NFA = " + (end1 - start1) + " ms");

		long start2 = System.currentTimeMillis();
		String suggestion2 = sugg.getBestSuggestion(word);
		if (suggestion1 != null) {
			System.out.println("Best match for " + word + " = " + suggestion2);
		} else {
			System.out.println("No suggestion found for " + word);
		}
		long end2 = System.currentTimeMillis();
		System.out.println("Benchmark for DFA = " + (end2 - start2) + " ms");
		
		//make sure the dfa and nfa give the same results
		assertTrue(suggestion2.equals(suggestion1));
	}
	
}

package ua.parser;

import java.io.BufferedReader;
import static java.nio.file.StandardCopyOption.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import ua.utility.Util;

public class Reader {
	
	public static String read(InputStream is) {
		Scanner sc = new Scanner(is);
		String txt = new String();
		while (sc.hasNextLine()) {
			txt += sc.nextLine();
		}
		sc.close();
		
		return txt;
	}
	
	/**
	 * Create a formatted wordCount file.
	 * @param input
	 * @param output
	 * @throws IOException
	 */
	public static void createFormattedFile(String input, String output) throws IOException {
		System.out.println("Formatting " + input);
		Map<String, Integer> wordCount = Reader.convert(new FileInputStream(input));
		Util.printMap(Util.cleanMap(wordCount), new FileOutputStream(output));
		System.out.println("Created formatted file: " + output);
	}
	
//	public static Map<String, Integer> addList(String input)

	/**
	 * Merge a list of files into single file
	 * @param maps List of files. These file should contain trigram lists.
	 * @return merged file.
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public static String merge(List<String> maps) throws FileNotFoundException, UnsupportedEncodingException {
		if (maps.size() == 2) {
//			System.out.println("Merging " + maps.get(0) + " with " + maps.get(1));
			Map<String, Integer> m = mergeMaps(Reader.getTrigramCounts(maps.get(0)), Reader.getTrigramCounts(maps.get(1)));
			String filename = maps.get(0);
			Reader.writeTrigramsToFile(m, filename);
			return filename;
			
		} else if (maps.size() == 1) {
//			System.out.println("Merging " + maps.get(0) + " with nothing");
			return maps.get(0);
			
		} else {
			List<String> m1 = maps.subList(0, (int)Math.floor(maps.size()/2));
			List<String> m2 = maps.subList((int)Math.floor(maps.size()/2), maps.size()-1);
			List<String> m1m2 = new LinkedList<String>();
			m1m2.add(Reader.merge(m1));
			m1m2.add(Reader.merge(m2));
			return merge(m1m2);
		}
	}
	
	/**
	 * Creates trigram file from @p input.
	 * @param input Existing file containing text to parse
	 * @param output
	 * @throws IOException
	 */
	public static void createTrigramFile(String input, String output) throws IOException {
		System.out.println("Creating trigrams file from " + input);
		List<String> tempFiles = Reader.getTrigrams(new FileInputStream(input));
		String result = Reader.merge(tempFiles);
		FileInputStream is = new FileInputStream(result);
		PrintWriter osw = new PrintWriter(output, "UTF-8");
		Scanner sc = new Scanner(is);
		while (sc.hasNextLine()) {
			osw.write(sc.nextLine() + "\n");
		}
		sc.close();
		osw.flush();
		osw.close();
		is.close();

		System.out.println("Trigramsfile " + output + " created.");
	}
	
	
	/**
	 * Merge 2 maps into single map.
	 * 
	 * Skip entries that occur only once to keep maps small enough.
	 * @param m1
	 * @param m2
	 * @return Merged map.
	 */
	private static Map<String, Integer> mergeMaps(Map<String, Integer> m1, Map<String, Integer> m2) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		for (Iterator<Map.Entry<String, Integer>> it = m1.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			String key1 = entry.getKey();
			// If in m1 and m2
			if (m2.containsKey(key1)) {
				if (m1.get(key1) > 1 || m2.get(key1) > 1 || m1.get(key1)+m2.get(key1) > 1) {
					map.put(key1, m1.get(key1) + m2.get(key1));
				} else {
					it.remove();
				}
			// if only in m1
			} else {
				if (m1.get(key1) > 1) {
					map.put(key1, m1.get(key1));
				}
			}
		}
		for (Iterator<Map.Entry<String, Integer>> it = m2.entrySet().iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			String key2 = entry.getKey();
			// if only in m2
			if (!m1.containsKey(key2)) {
				if (m2.get(key2) > 1) {
					map.put(key2, m2.get(key2));
				}
			}
		}
		return map;
	}
	
	/**
	 * Write map of trigrams to @p output file.
	 * @param trigrams
	 * @param output
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	private static void writeTrigramsToFile(Map<String, Integer> trigrams, String output) throws FileNotFoundException, UnsupportedEncodingException {
		PrintWriter osw = new PrintWriter(output, "UTF-8");
		for (String key : trigrams.keySet()) {
			osw.write(key + " = " + trigrams.get(key) + "\n");
		}
		osw.flush();
		osw.close();
	}
	
	private static String getTempDir() {
	    String property = "java.io.tmpdir";
	    String tempDir = System.getProperty(property);
	    return tempDir;
	}
	
	/**
	 * Parse file and return list of partial trigram file names.
	 * @param is
	 * @return List of names of partial trigram files.
	 * @throws IOException
	 */
	private static List<String> getTrigrams(InputStream is) throws IOException {
		LinkedList<String> result = new LinkedList<String>();
		Map<String, Integer> trigramCount = new HashMap<String, Integer>();
		Scanner sc = new Scanner(is, "UTF-8");
		sc.useDelimiter("[^A-Za-z]");
		String t1 = null, t2 = null, t3 = null;
		Integer mapCount = 0;
		while(sc.hasNext()) {
			if (trigramCount.size() > 1000000) {
				// write to file and continue with new map
				String tmpFile = Reader.getTempDir() + "trigrams_tmp_" + mapCount++ + ".txt";
				Reader.writeTrigramsToFile(trigramCount, tmpFile);
				result.add(tmpFile);
				trigramCount.clear();
			}
			String token = sc.next();
			// Skip empty token.
			if (token.equals(" ") || token.equals("")) {
				continue;
			}
			// Make lower case
			token = token.toLowerCase();
			t1 = t2;
			t2 = t3;
			t3 = token;
			if (t1 != null && t2 != null && t3 != null) {
				String trigram = t1 + " " + t2 + " " + t3;
				if (!trigramCount.containsKey(trigram)) {
					trigramCount.put(trigram, 1);
				} else {
					trigramCount.put(trigram, trigramCount.get(trigram)+1);
				}
			}
		}
		
		// write to file
		String tmpFile = "trigrams_tmp_" + mapCount + ".txt";
		Reader.writeTrigramsToFile(trigramCount, tmpFile);
		result.add(tmpFile);
		trigramCount.clear();
		
		if (is != null)
			is.close();
		if (sc != null)
			sc.close();
		
		return result;
	}
	
	/**
	 * Convert inputstream to wordCount map.
	 * @param is
	 * @return Map containing the words and their counts.
	 */
	private static Map<String, Integer> convert(InputStream is) {
		Map<String, Integer> wordCount = new HashMap<String, Integer>();
		Scanner sc = new Scanner(is);
		sc.useDelimiter("[^A-Za-z]");
		while (sc.hasNext()) {
			// Get next token.
			String token = sc.next();
			// Skip empty token.
			if (token.equals(" ") || token.equals("")) {
				continue;
			}
			// Make lower case
			token = token.toLowerCase();
			
			if (!wordCount.containsKey(token)) {
				wordCount.put(token, 1);
			} else {
				wordCount.put(token, wordCount.get(token)+1);
			}
		}
		sc.close();
		
		return wordCount;
	}
	
	/**
	 * Read map from formatted wordCount files.
	 * @param input
	 * @return
	 */
	public static Map<String, Integer> getCounts(String input) {
		System.out.println("Getting counts from " + input);
		Map<String, Integer> r = new HashMap<String, Integer>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(input));
		    String line = null;
		    while ((line = br.readLine()) != null) {
		    	String[] l = line.split(" ");
		    	r.put(l[0], Integer.parseInt(l[l.length-1]));
		    }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Found all counts");
		return r;
	}
	
	/**
	 * Read map from formatted trigram files.
	 * @param input
	 * @return
	 */
	public static Map<String, Integer> getTrigramCounts(String input) {
//		System.out.println("Getting counts from " + input);
		//We use a treemap for trigrams so that we can search for partial keys much faster.
		Map<String, Integer> r = new TreeMap<String, Integer>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(input));
		    String line = null;
		    while ((line = br.readLine()) != null) {
		    	String[] l = line.split(" ");
		    	r.put(l[0] + " " + l[1] + " " + l[2], Integer.parseInt(l[l.length-1]));
		    }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("Found all counts");
		return r;
	}
	
}

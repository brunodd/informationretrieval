package ua.algorithms;

public class Levenshtein {
	//Wagner–Fischer algorithm
	
	public static int calculateDistance(String one, String two) {
		
		char[] array_one = one.toCharArray();
		char[] array_two = two.toCharArray();
		
        int len1 = one.length();
        int len2 = two.length();
        
        int[][] matrix = new int[len1 + 1][len2 + 1];
        
        for (int i = 0; i <= len1; i++)
            matrix[i][0] = i;
        for (int i = 1; i <= len2; i++)
            matrix[0][i] = i;
        
        for (int i = 0; i < len1; i++)
        {
            for (int j = 0; j < len2; j++)
            {
            	if (array_one[i] == array_two[j])
            		matrix[i+1][j+1] = matrix[i][j];
            	else {
            		matrix[i+1][j+1] =  Math.min(matrix[i][j + 1] + 1,  //deletion
            				Math.min(matrix[i + 1][j] + 1,				//insertion
            						matrix[i][j] + 1));					//substitution
            	}
            }
        }
        return matrix[len1][len2];
	}
}

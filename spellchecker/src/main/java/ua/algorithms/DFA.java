package ua.algorithms;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import ua.algorithms.NFA.TRANS;

public class DFA {
	
	private String startnode;
	private HashSet<String> nodes;
	private HashSet<String> endNodes;
	private Map<Pair<String,Character>,String> transitions;
	
	public DFA() {
		this.nodes = new HashSet<String>();
		this.endNodes = new HashSet<String>();
		this.transitions = new HashMap<Pair<String,Character>,String>();
	}
	
	//Build a dfa from a NFA
	public DFA(NFA auto) {
		this.buildFromNFA(auto);	
	}
	
	
	//set accept to true if this is an accept state
	public void addNode(String n, boolean accept) {
		if (accept)
			endNodes.add(n);
		nodes.add(n);
	}
	
	public void addTransition(String n1, char trans, String n2) {
		if (nodes.contains(n1) && nodes.contains(n2)) {
			Pair<String,Character> key = new Pair<String,Character>(n1,trans);
			if (!transitions.containsKey(key)) {
				transitions.put(key, n2);
			} else {
				System.err.println("No duplicate transitions allowed");
			}
			
		}
		else
			System.err.println("Illegal transition. One of (" + n1 + "," + n2 + ") does not exist.");
	}
	
	public void addTransition(String n1, TRANS trans, String n2) {
		this.addTransition(n1, trans.ch(), n2);
	}
	
	
	public String next(String n, char trans) {
		
		if (nodes.contains(n)) {
			HashSet<String> results = new HashSet<String>();
			//get the node reachable by any character
			String next = transitions.get(new Pair<String,Character>(n,trans));
			if (next != null)
				return next;
			else return transitions.get(new Pair<String,Character>(n,TRANS.ALL.ch()));
		}
		return null;
	}
	
	public String next(String n, TRANS trans) {
		return next(n,trans.ch());
	}
	
	public void toFile(String path) {
		try {
			PrintWriter fout= new PrintWriter(path);
			fout.println("digraph g {");
			if (endNodes.size() > 0) {
				fout.println("{");
				for (String node: endNodes) {
					fout.println("\"" + node + "\" [shape=doublecircle]");
				}
				fout.println("}");

			}
			
			for (Pair<String,Character> p : this.transitions.keySet()) {
				String node = transitions.get(p);
				
				String line = "\"" + p.one() + "\" -> \"" + node + 
						"\" [label=\"" + p.two() + "\"]";
				fout.println(line);				
			}
			
			fout.println("}");
			fout.close();
		} catch (FileNotFoundException e) {
			System.err.println(path + " not found");
		}
	}
	
	public boolean match(String expression) {
		String currentNode = this.startnode;
		//System.out.println(currentNode);
		for (char c : expression.toCharArray()) {
			String newNode = next(currentNode,c);
			//System.out.println(newNode);
			if (newNode == null)
				return false;
			currentNode = newNode;
			
		}
		if (this.endNodes.contains(currentNode))
			return true;
	
		return false;
	}
	
	public void buildFromNFA(NFA auto) {
		
		
		
	}


	public String getStartnode() {
		return startnode;
	}


	public HashSet<String> getNodes() {
		return nodes;
	}


	public HashSet<String> getEndNodes() {
		return endNodes;
	}


	public Map<Pair<String, Character>, String> getTransitions() {
		return transitions;
	}

	public void setStartnode(String startnode) {
		if (!this.nodes.contains(startnode))
			this.nodes.add(startnode);
		this.startnode = startnode;
	}

	public void setNodes(HashSet<String> nodes) {
		this.nodes = nodes;
	}

	public void setEndNodes(HashSet<String> endNodes) {
		this.endNodes = endNodes;
	}

	public void setTransitions(Map<Pair<String, Character>, String> transitions) {
		this.transitions = transitions;
	}
	
	
	
}

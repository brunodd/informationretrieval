package ua.algorithms;

import ua.algorithms.NFA.TRANS;

public class LevenshteinAutomaton {

	private DFA dfa;
	private NFA nfa;
	private String word;
	private int maxDist;
	
	public LevenshteinAutomaton(String word, int maxDist) {
		assert(word != null && word != "");
		
		this.maxDist = maxDist;
		this.word = word;
		
		this.nfa = null;
		
		this.generateAutomaton();
	}

	private void generateAutomaton() {
		generateAutomaton(this.word);
	}
	
	private void generateAutomaton(String word) {
		this.nfa = new NFA("0,0",false);
		for (int j=0; j<word.length(); j++) {
			for (int i=0; i< maxDist; i++) {
				int next = j+1;
				int up = i + 1;
				String insert = j + "," + up;
				String edit = next + "," + up;
				String correct = next + "," + i;
				this.nfa.addNode(insert, false);
				
				if(next == word.length()) {
					this.nfa.addNode(edit, true);
					this.nfa.addNode(correct, true);
				} else {
					this.nfa.addNode(edit, false);
					this.nfa.addNode(correct, false);
				}
				

				
				String currentNode = j+","+i;

				this.nfa.addTransition(currentNode, TRANS.ALL, insert);
				this.nfa.addTransition(currentNode, TRANS.EPSILON, edit);
				this.nfa.addTransition(currentNode, TRANS.ALL, edit);
				this.nfa.addTransition(currentNode, word.toCharArray()[j], correct);
			}
		}
		for (int j=0; j<word.length(); j++) {
			String currentNode = j+","+ maxDist;
			int next = j+1;
			String correct = next + "," + maxDist;
			this.nfa.addTransition(currentNode, word.toCharArray()[j], correct);

		}
		for (int i=0; i< maxDist; i++) {
			int up = i + 1;
			String insert = word.length() + "," + up;
			String currentNode = word.length() + "," + i;
			this.nfa.addTransition(currentNode, TRANS.ALL, insert);

		}
		
		this.dfa = this.nfa.toDFA();
	}
	
	public void printAutomaton(String path) {
		this.nfa.toFile(path);
	}
	
	
	public boolean matches(String word) {
		if (!this.canMatch(word)) {
			return false;
		}

		return this.dfa.match(word.toLowerCase());
	}
	
	public boolean matchesUsingNFA(String word) {
		if (!this.canMatch(word)) {
			return false;
		}

		return this.nfa.match(word.toLowerCase());
	}
	
	private boolean canMatch(String word) {
		// Improvement 1: check lengths of words
		if (Math.abs((word.length()-this.word.length())) > this.maxDist) {
			return false;
		}
		
		return true;
	}
}

package ua.algorithms;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class NFA {

	public enum TRANS {
		EPSILON('\u03B5'),
		ALL('*');
		
		private char c;
		
		TRANS(char c) {
			this.c = c;
		}
		
		public char ch() {
			return this.c;
		}
	}
	
	private String startnode;
	private HashSet<String> nodes;
	private HashSet<String> endNodes;
	private Map<Pair<String,Character>,HashSet<String>> transitions;
	
	public NFA(String startnode, boolean accept) {
		this.startnode = startnode;
		this.nodes = new HashSet<String>();
		this.endNodes = new HashSet<String>();
		addNode(startnode,accept);
		this.transitions = new HashMap<Pair<String,Character>, HashSet<String>>();
	}
	
	//set accept to true if this is an accept state
	public void addNode(String n, boolean accept) {
		if (accept)
			endNodes.add(n);
		nodes.add(n);
	}
	
	public void addTransition(String n1, char trans, String n2) {
		if (nodes.contains(n1) && nodes.contains(n2)) {
			Pair<String,Character> key = new Pair<String,Character>(n1,trans);
			if (transitions.containsKey(key)) {
				transitions.get(key).add(n2);
			} else {
				HashSet<String> nodes = new HashSet<String>();
				nodes.add(n2);
				transitions.put(key, nodes);
			}
			
		}
		else
			System.err.println("Illegal transition. One of (" + n1 + "," + n2 + ") does not exist.");
	}
	
	public void addTransition(String n1, TRANS trans, String n2) {
		this.addTransition(n1, trans.ch(), n2);
	}
	
	
	public HashSet<String> next(String n, char trans) {
		
		if (nodes.contains(n)) {
			HashSet<String> results = new HashSet<String>();
			//get all the nodes reachable by any character
			if (transitions.get(new Pair<String,Character>(n,TRANS.ALL.ch())) != null)
				results.addAll(transitions.get(new Pair<String,Character>(n,TRANS.ALL.ch())));
			//all the nodes reachable with this character
			if (transitions.get(new Pair<String,Character>(n,trans)) != null)
				results.addAll(transitions.get(new Pair<String,Character>(n,trans)));
			if (results != null)
				return results;
		}
		return new HashSet<String>();
	}
	
	public HashSet<String> next(String n, TRANS trans) {
		return next(n,trans.ch());
	}
	
	public void toFile(String path) {
		try {
			PrintWriter fout= new PrintWriter(path);
			fout.println("digraph g {");
			if (endNodes.size() > 0) {
				fout.println("{");
				for (String node: endNodes) {
					fout.println("\"" + node + "\" [shape=doublecircle]");
				}
				fout.println("}");

			}
			
			for (Pair<String,Character> p : this.transitions.keySet()) {
				for (String node : transitions.get(p)) {
					String two = p.two().toString();
					
					String line = "\"" + p.one() + "\" -> \"" + node + 
							"\" [label=\"" + two + "\"]";
					fout.println(line);
				}
			}
			
			fout.println("}");
			fout.close();
		} catch (FileNotFoundException e) {
			System.err.println(path + " not found");
		}
	}
	
	public boolean match(String expression) {
		HashSet<String> currentNodes = new HashSet<String>();
		currentNodes.add(this.startnode);
		
		
		//add all epsilon transitions to current set
		currentNodes = doAllEpsilonTransitions(currentNodes);
		
		HashSet<String> newNodes = new HashSet<String>();
		for (char c : expression.toCharArray()) {
			newNodes = new HashSet<String>();
			//use iterator for safe remove
			Iterator<String> i = currentNodes.iterator();
			while (i.hasNext()) {
				String node = i.next();
				newNodes.addAll(next(node,c));
				i.remove();
			}
			currentNodes.addAll(newNodes);
			
			//again do all epsilon transitions while set is changing
			currentNodes = doAllEpsilonTransitions(currentNodes);
			
			if(currentNodes.isEmpty())
				return false;

			//System.out.println("char '" + c + "' :");
			//System.out.println(currentNodes.toString());
			
		}
		
		// if at least one state is an accept state, true
		for (String node :  currentNodes) {
			if (this.endNodes.contains(node))
				return true;
		}
		return false;
	}
	
//	private boolean canMatch(String node) {
//		if (node.matches())
//	}
	
	private HashSet<String> doAllEpsilonTransitions(HashSet<String> currentNodes) {
		HashSet<String> newNodes = new HashSet<String>();
		int size = currentNodes.size();
		
		for (String node : currentNodes) {
			newNodes.addAll(next(node,TRANS.EPSILON));
		}
		currentNodes.addAll(newNodes);
		
		while (currentNodes.size() > size) {
			newNodes = new HashSet<String>();
			size = currentNodes.size();
			
			for (String node : currentNodes) {
				newNodes.addAll(next(node,TRANS.EPSILON));
			}
			currentNodes.addAll(newNodes);				
		}
		return currentNodes;
	}
	
	public DFA toDFA() {
		DFA result = new DFA();
		
		HashSet<String> currentNodes = new HashSet<String>();
		currentNodes.add(this.startnode);
		
		HashMap<String,HashSet<String>> dfaNodeToNFAset = new HashMap<String,HashSet<String>>();
		HashMap<String,Boolean> marked = new HashMap<String,Boolean>();
		
		//add all epsilon transitions to current set
		currentNodes = doAllEpsilonTransitions(currentNodes);
		
		//if the two are not disjoint, it means they have at least one item in common
		boolean acceptnode = !Collections.disjoint(this.endNodes,currentNodes);
		result.addNode(currentNodes.toString(),acceptnode);
		result.setStartnode(currentNodes.toString());
		dfaNodeToNFAset.put(currentNodes.toString(), currentNodes);
		
		HashSet<Character> language = this.getLanguage();
		HashSet<String> allDFANodes = new HashSet<String>();
		allDFANodes.add(currentNodes.toString());
		
		boolean repeat = true;
		while (repeat) {
			repeat = false;
			for (String node : allDFANodes) {
				//System.out.println(node);
				
				if(marked.get(node) == null || !marked.get(node)) {
					marked.put(node, true);
					HashSet<String> nfaNodes = dfaNodeToNFAset.get(node);
					assert(nfaNodes != null);
					
					
					for(char c : language) {
						if (c == TRANS.EPSILON.ch())
							continue;
						HashSet<String> nextNodes = doAllEpsilonTransitions(move(nfaNodes,c));
						if (nextNodes.size() > 0) {
							dfaNodeToNFAset.put(nextNodes.toString(),nextNodes);
							
							acceptnode = !Collections.disjoint(this.endNodes,nextNodes);
							result.addNode(nextNodes.toString(),acceptnode);
							repeat = true;
							result.addTransition(node, c, nextNodes.toString());
						}
					}

				}
				
			}
			allDFANodes.addAll(result.getNodes());
			
		}

		return result;
	}
	
	public HashSet<Character> getLanguage() {
		HashSet<Character> language = new HashSet<Character>();
		for (Pair<String,Character> key: this.transitions.keySet()) {
			language.add(key.two());
		}
		
		return language;
	}
	
	public HashSet<String> move(HashSet<String> nodes, char c) {
		HashSet<String> next = new HashSet<String>();
		
		for (String node : nodes) {
			next.addAll(next(node,c));
		}
		
		return next;
	}

}

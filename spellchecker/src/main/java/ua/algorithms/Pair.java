package ua.algorithms;

public class Pair<T,G> {

	private T _1;
	private G _2;
	
	Pair() {
		
	}
	
	public Pair(T one, G two) {
		this._1 = one;
		this._2 = two;
	}
	
	public T one() {
		return this._1;
	}
	
	public G two() {
		return this._2;
	}
	
	public String toString() {
		return "(" + this._1.toString() + "," + this._2.toString() + ")";
	}
	
	@Override
	public int hashCode() {
		return this._1.hashCode() + this._2.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		Pair<T,G> o = (Pair<T,G>) obj;

		return this._1.equals(o.one()) && this._2.equals(o.two());
	}
}

package ua.utility;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Util {
	
	public static void printMap(Map map, OutputStream os) throws IOException {
		OutputStreamWriter w = new OutputStreamWriter(os);
	    Iterator it = map.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>)it.next();
			w.write(pair.getKey() + " = " + pair.getValue() + "\n");
	        it.remove(); // avoids a ConcurrentModificationException
		}
	    w.flush();
	    w.close();
	}
	
	public static Map cleanMap(Map map) {
		Map<String, Integer> m = new HashMap<String, Integer>();
	    Iterator it = map.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<String, Integer> pair = (Map.Entry<String, Integer>)it.next();
	    	if (pair.getValue() < 100) {
	    		it.remove();
	    		continue;
	    	}
	        m.put(pair.getKey(), pair.getValue());
	        it.remove(); // avoids a ConcurrentModificationException
		}
	    return m;
	}
	
	public static String[] stripString(String s) {
		s = s.replaceAll("[^A-Za-z]", " ");
		String[] tokens = s.split("[ ]+");
		// Skip empty token.
		if (s.equals(" ") || s.equals("")) {
			return null;
		}
		// Make lower case
		for (int i=0; i<tokens.length; ++i) {
			tokens[i] = tokens[i].toLowerCase();
		}

		return tokens;
	}
}

package ua.spellchecker;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import ua.algorithms.Pair;

public class NaiveSuggestor implements Suggestor {
	
  final static Set<String> ALPHABET = new HashSet<String>(
		  Arrays.asList(new String[]{"a", "b", "c", "d", "e", "f", "g",
    "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
    "w", "x", "y", "z"}
		  ));
	
	Map<String, Integer> wordCounts;
	public NaiveSuggestor(Map<String, Integer> wc) {
		this.wordCounts = wc;
	}
  
  	private Set<String> deletes(String word) {
  		Set<String> results = new HashSet<String>();
  		for (int i=0; i<word.length(); ++i) {
  	  		StringBuilder sb = new StringBuilder(word);
  			results.add(sb.deleteCharAt(i).toString());
  		}
  		return results;
  	}
  	
  	private Set<String> inserts(String word) {
  		Set<String> results = new HashSet<String>();
  		
  		for (int i=0; i<(word.length()+1); ++i) {
  			for (String c : ALPHABET) {
  		  		StringBuilder sb = new StringBuilder(word);
  	  			results.add(sb.insert(i, c).toString());
  			}
  		}
  		
  		return results;
  	}
  	
  	private Set<String> swaps(String word) {
  		Set<String> results = new HashSet<String>();
  		for (int i=0; i<word.length(); ++i) {
  			for (int j=0; j<word.length(); ++j) {
  				StringBuilder sb = new StringBuilder(word);
  				char ci = sb.charAt(i);
  				sb.setCharAt(i, sb.charAt(j));
  				sb.setCharAt(j, ci);
  				results.add(sb.toString());
  			}
  		}
  		return results;
  	}
  	
  	private Set<String> substitutions(String word) {
  		Set<String> results = new HashSet<String>();
  		
  		for (int i=0; i<word.length(); ++i) {
  			for (String c : ALPHABET) {
  		  		StringBuilder sb = new StringBuilder(word);
  				sb.setCharAt(i, c.charAt(0));
  				results.add(sb.toString());
  			}
  		}
  		return results;
  	}
  	
	public HashSet<String> getSuggestions(String word) { 
		HashSet<String> result = new HashSet<String>();
		
		result.addAll(this.substitutions(word));
		result.addAll(this.deletes(word));
		result.addAll(this.inserts(word));
		result.addAll(this.swaps(word));
		
		return result;
	}
	
	public HashSet<String> getSuggestions2(String word) {
		HashSet<String> result = new HashSet<String>();
		HashSet<String> edit1 = getSuggestions(word);
		Iterator<String> it = edit1.iterator();
		while(it.hasNext()) {
			String w = it.next();
			result.addAll(getSuggestions(w));
		}
		return result;
	}

	public HashSet<String> getSuggestions3(String word) {
		HashSet<String> result = new HashSet<String>();
		HashSet<String> edit1 = getSuggestions2(word);
		Iterator<String> it = edit1.iterator();
		while(it.hasNext()) {
			String w = it.next();
			result.addAll(getSuggestions(w));
		}
		return result;
	}
	
	public String getBestSuggestion(String word) {
		String r = null;
		for (int i = 1; i < 4; ++i) {
			r = this.getBestSuggestion(word, i);
			if (r != null) { 
				break;
			}
		}
		return r;
	}
	
	
	/* (non-Javadoc)
	 * @see ua.spellchecker.Suggestor#getBestSuggestion(java.lang.String, ua.spellchecker.TermChecker)
	 */
	public String getBestSuggestion(String word, int edit) {
		if (wordCounts.containsKey(word)) return word;
		Set<String> suggestions = null;
		switch(edit) {
		case 1:
			suggestions = this.getSuggestions(word);
			break;
		case 2:
			suggestions = this.getSuggestions2(word);
			break;
		case 3:
			suggestions = this.getSuggestions3(word);
			break;
		default:
			return word;
		}
		Iterator<String> it = suggestions.iterator();
		Pair<String, Integer> bestMatch = new Pair<String, Integer>("",0);
		while(it.hasNext()) {
			String s = it.next();
			if (wordCounts.containsKey(word)) {
				Integer occ = wordCounts.get(word);
				if (occ > bestMatch.two()) {
					bestMatch = new Pair<String, Integer>(s, occ);
				}
			}
		}
		
		if (bestMatch.two() > 0) {
			return bestMatch.one();
		} else {
			return null;
		}
	}
}

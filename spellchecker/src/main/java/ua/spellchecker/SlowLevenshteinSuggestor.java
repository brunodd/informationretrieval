package ua.spellchecker;

import java.util.Map;

import ua.algorithms.Levenshtein;
import ua.algorithms.Pair;

public class SlowLevenshteinSuggestor implements Suggestor {
	Map<String, Integer> wordCounts;
	
	public SlowLevenshteinSuggestor(Map<String,Integer> wc) {
		this.wordCounts = wc;
	}
	
	@Override
	public String getBestSuggestion(String word) {
		if (this.wordCounts.containsKey(word))
			return word;
		
		
		for (int i=1; i<5 ; i++) {
			Pair<String, Integer> bestMatch = new Pair<String, Integer>("", 0);
			for (String correction : this.wordCounts.keySet()) {
				if (Levenshtein.calculateDistance(word, correction) <= i) {
					if (this.wordCounts.get(correction) > bestMatch.two()) {
						bestMatch = new Pair<String, Integer>(correction, this.wordCounts.get(correction));
					}
				}
				
			}
			if (bestMatch.two() > 0) 
				return bestMatch.one();
		}
		return word;
	}

}

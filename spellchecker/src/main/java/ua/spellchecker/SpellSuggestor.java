package ua.spellchecker;

import java.util.Map;

public class SpellSuggestor implements Suggestor {
	private TriGramSuggestor tg_sugg;
	private LevenshteinSuggestor ls_sugg;
	
	public SpellSuggestor(Map<String, Integer> tc, Map<String,Integer> wc) {
		this.tg_sugg = new TriGramSuggestor(tc, wc);
		this.ls_sugg = new LevenshteinSuggestor(wc);
	}

	@Override
	public String getBestSuggestion(String phrase) {
		if(phrase.replace("[[{Punct}]]", "") == "") {
			return phrase;
		} else {
			// perform term correction on each word
			String[] words = this.splitInWords(phrase);
			String intermediate = "";
			for (int word_index = 0; word_index < words.length; word_index++) {
				String w = words[word_index];
				intermediate += this.ls_sugg.getBestSuggestion(w);
				if (word_index != words.length-1) {
					intermediate += " ";
				}
			}
			if (!isSentence(phrase)) {
				return intermediate;
			} else {
				// if it is 3 words or longer, perform context sensitive correction.
				return this.tg_sugg.analyseSentence(intermediate);
			}
		}
	}

	/**
	 * A phrase is a sentence if it contains 3 words or more.
	 * @param phrase
	 * @return
	 */
	private Boolean isSentence(String phrase) {
		return (this.splitInWords(phrase).length >= 3);
	}
	
	/**
	 * Split a phrase in an array of words.
	 * @param phrase
	 * @return
	 */
	private String[] splitInWords(String phrase) {
		return phrase.split("[\\p{Punct}\\s]+");
	}
}

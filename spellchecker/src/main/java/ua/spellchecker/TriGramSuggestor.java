package ua.spellchecker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

import ua.algorithms.LevenshteinAutomaton;
import ua.algorithms.Pair;

public class TriGramSuggestor implements Suggestor {
	Map<String,Integer> wordCounts;
	TreeMap<String, Integer> triCounts;
	
	public TriGramSuggestor(Map<String, Integer> tc, Map<String,Integer> wc) {
		try {
			this.triCounts = (TreeMap<String, Integer>) tc;
			this.wordCounts = wc;
		} catch(Exception e) {
			System.err.println("The trigram map should be structured as a TreeMap.");
		}
	}

	@Override
	public String getBestSuggestion(String trigram) {
		return getBestTrigram(trigram).one();
	}
	
	public Set<String> getConfusionSet(String word, int editdistance) {
		Set<String> result = new HashSet<String>();
		LevenshteinAutomaton automaton = new LevenshteinAutomaton(word, editdistance);
		
		for (Entry<String, Integer> e : this.wordCounts.entrySet()) {
			if (automaton.matches(e.getKey())) result.add(e.getKey());
		}
		
		return result;
	}
	
	private Pair<String, Integer> getBestTrigram(String trigram) {
		String [] words = trigram.split(" ");
		assert(words.length == 3) : trigram + " must contain exactly 3 words.";
		// if trigram in database -> correct 
		if (triCounts.containsKey(trigram)){
			return new Pair<String,Integer>(trigram,triCounts.get(trigram));
		} else {
			for (int i=1; i< 4; i++) {
				HashSet<String> trigramSet = new HashSet<String>();
				
				//For every word in tri-gram: find all words in confusion set
				for (String w: this.getConfusionSet(words[0], i)) 
					trigramSet.add(w + " " + words[1] + " " + words[2]);
				
				for (String w: this.getConfusionSet(words[1], i)) 
					trigramSet.add(words[0] + " " + w + " " + words[2]);
				
				for (String w: this.getConfusionSet(words[2], i)) 
					trigramSet.add(words[0] + " " + words[1] + " " + w);
				
				//System.out.println(trigramSet);
				Pair<String, Integer> bestTrigram = new Pair<String, Integer>("",-1);
				
				for (String suggestion : trigramSet) {
					Integer count = triCounts.get(suggestion);
					//better suggestion
					if (count != null && count > bestTrigram.two()) {
						bestTrigram = new Pair<String, Integer>(suggestion, count);
					}
				}
				//try to account for edit distance in score by dividing by 5^editDistance
				if (bestTrigram.two() > 0) {
					int score = (int) ((int) bestTrigram.two()/Math.pow(5, i));
					return new Pair<String, Integer> (bestTrigram.one(), score);
				}
			}
		}
		return new Pair<String, Integer>("",-1);		
	}
	
	public String analyseSentence(String sentence) {
		String[] words = sentence.toLowerCase().split("[\\p{Punct}\\s]+");
		assert(words.length >= 3) : sentence + " must contain at least 3 words.";
		
		ArrayList<String> allTrigrams = new ArrayList<String>();
		
		for (int i=0; i<words.length-2; i++) {
			allTrigrams.add(words[i] + " " + words[i+1] + " " + words[i+2]);
		}

		ArrayList<Pair<String,Integer>> corrections = new ArrayList();
		for (String trigram : allTrigrams) {
			corrections.add(this.getBestTrigram(trigram));
		}
		
		/*
		for each position keep pairs (word,score)
		if word in t1, score += score(t1)
			else
			new word at this position with score(t1)
			
		for each position return word with highest score
		*/
		String[] bestWords = words;
		ArrayList<HashMap<String,Integer>> wordScores = new ArrayList();
		for (String word : words){
			HashMap<String,Integer> scores = new HashMap<String,Integer>();
			scores.put(word, 5);
			wordScores.add(scores);
		}
		for (int i = 0; i< corrections.size(); i++) {
			Pair<String,Integer> trigram = corrections.get(i);
			if (trigram.two() >= 0) {
				String[] triwords = trigram.one().split(" ");
				if(wordScores.get(i).containsKey(triwords[0])) {
					wordScores.get(i).put(triwords[0], wordScores.get(i).get(triwords[0]) + trigram.two());
				} else {
					wordScores.get(i).put(triwords[0],trigram.two());
				}
				if(wordScores.get(i+1).containsKey(triwords[1])) {
					wordScores.get(i+1).put(triwords[1], wordScores.get(i+1).get(triwords[1]) + trigram.two());
				} else {
					wordScores.get(i+1).put(triwords[1],trigram.two());
				}
				if(wordScores.get(i+2).containsKey(triwords[2])) {
					wordScores.get(i+2).put(triwords[2], wordScores.get(i+2).get(triwords[2]) + trigram.two());
				} else {
					wordScores.get(i+2).put(triwords[2],trigram.two());
				}
					
				
			}
		}
		for (int i=0; i<wordScores.size(); i++) {
			HashMap<String,Integer> hm = wordScores.get(i);
			Pair<String,Integer> highest = new Pair<String, Integer>("", -1);
			for (Entry<String,Integer> e : hm.entrySet()) {
				if (e.getValue() > highest.two())
					highest = new Pair<String, Integer>(e.getKey(), e.getValue());
			}
			bestWords[i] = highest.one();
		}
		String result = "";
		for (int i = 0; i < bestWords.length; ++i) {
			String w = bestWords[i];
			result += w;
			if (i != bestWords.length-1) {
				result += " ";
			}
		}
		
		return result;
	}

}
/*
 * 			
			String[] tri = trigram.split(" ");
			assert(tri.length > 1);
			
			//take a submap of all the pairs of which the key starts with the first two words from the trigram
			String start = trigram.split(" ")[0] + " " + trigram.split(" ")[1];
			String end = trigram.split(" ")[0] + " " + trigram.split(" ")[1] + " zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
			
			Map<String,Integer> sub = triCounts.subMap(start,end);
			String best = "";
			int max = -1;
			for (String key : sub.keySet()) {
				int frequency = sub.get(key);
				if (frequency > max) {
					max = frequency;
					best = key;
				}
			}
			return best;
		}
 * 
 */
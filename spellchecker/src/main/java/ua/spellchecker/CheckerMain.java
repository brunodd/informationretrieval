package ua.spellchecker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import ua.parser.Reader;

/**
 * Spellchecker class
 */
public class CheckerMain 
{
//	final static String BIGTEXT = "./resources/bigtext.txt";
//	final static String BIGTEXT = "./resources/collection.txt";
	final static String BIGTEXT = "/Users/brunodd/Documents/results.txt";
	final static String OUTPUT = "./resources/wordsList.txt";
	final static String TRIGRAMS = "./resources/bnc_trigrams.txt";
    public static void main( String[] args )
    {
    	
        try {
        	File f = new File(OUTPUT);
        	if((f.exists() && !f.isDirectory()) == false) { 
            	Reader.createFormattedFile(BIGTEXT, OUTPUT);
        	}
        	
        	
			Map<String, Integer> wordCount = Reader.getCounts(OUTPUT);
			
			Suggestor s1 = new NaiveSuggestor(wordCount);
			Suggestor s2 = new LevenshteinSuggestor(wordCount);
			
			String original = "pratnershi";

			// Naive
			long start1 = System.currentTimeMillis();
			String suggestion1 = s1.getBestSuggestion(original);
			if (suggestion1 != null) {
				System.out.println("Best match for " + original + " = " + suggestion1);
			} else {
				System.out.println("No suggestion found for " + original);
			}
			long end1 = System.currentTimeMillis();
			System.out.println("Benchmark for Naive = " + (end1 - start1) + " ms");
			

			// Levenshtein
			long start2 = System.currentTimeMillis();
			String suggestion2 = s2.getBestSuggestion(original);
			if (suggestion2 != null) {
				System.out.println("Best match for " + original + " = " + suggestion2);
			} else {
				System.out.println("No suggestion found for " + original);
			}
			long end2 = System.currentTimeMillis();
			System.out.println("Benchmark for Levenshtein = " + (end2 - start2) + " ms");
			
			
//			LevenshteinAutomaton au = new LevenshteinAutomaton("someword", 1);
//			System.out.println(au.matches("somword") ? "match" : "no match");
			
//			tc.simulate();

//			Util.printMap(wordCount, new FileOutputStream(OUTPUT));
			


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}

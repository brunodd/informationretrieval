package ua.spellchecker;

import java.util.Map;
import java.util.Map.Entry;

import ua.algorithms.LevenshteinAutomaton;
import ua.algorithms.Pair;

public class LevenshteinSuggestor implements Suggestor {
	Map<String, Integer> wordCounts;
	public LevenshteinSuggestor(Map<String, Integer> wc) {
		this.wordCounts = wc;
	}
	
	public String getBestSuggestion(String word) {
		if (wordCounts.containsKey(word)) return word;
		for (int i=1; i<5; ++i) {
			LevenshteinAutomaton au = new LevenshteinAutomaton(word, i);
			au.printAutomaton("./automaton" + i + ".dot");
			Pair<String, Integer> bestMatch = new Pair<String, Integer>("", 0);
			for (Entry<String, Integer> e : this.wordCounts.entrySet()) {
				if (au.matches(e.getKey())) {
					if (e.getValue() > bestMatch.two()) {
						bestMatch = new Pair<String, Integer>(e.getKey(), e.getValue());
					}
				}
			}
			if (bestMatch.two() > 0) {
				return bestMatch.one();
			}
		}
		//if no correction is found, return the unknown word
		return word;
	}
	
	public String getBestSuggestionWithNFA(String word) {
		if (wordCounts.containsKey(word)) return word;
		for (int i=1; i<5; ++i) {
			LevenshteinAutomaton au = new LevenshteinAutomaton(word, i);
			au.printAutomaton("./automaton" + i + ".dot");
			Pair<String, Integer> bestMatch = new Pair<String, Integer>("", 0);
			for (Entry<String, Integer> e : this.wordCounts.entrySet()) {
				if (au.matchesUsingNFA(e.getKey())) {
					if (e.getValue() > bestMatch.two()) {
						bestMatch = new Pair<String, Integer>(e.getKey(), e.getValue());
					}
				}
			}
			if (bestMatch.two() > 0) {
				return bestMatch.one();
			}
		}
		//if no correction is found, return the unknown word
		return word;
	}
}

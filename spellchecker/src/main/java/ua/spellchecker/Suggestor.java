package ua.spellchecker;

public interface Suggestor {

	String getBestSuggestion(String word);

}
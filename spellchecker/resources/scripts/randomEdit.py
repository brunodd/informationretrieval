import sys
import random

if (len(sys.argv) < 3):
    print "usage:\npython randomEdit.py <listofwords.txt> <editdistance>"
    sys.exit(0)

filename = sys.argv[1]
maxeditdistance = int(sys.argv[2])

def replace(word):
    r = random.randint(97,122)
    newchar = chr(r)
    index = random.randint(0,len(word)-1)
    return word[:index] + newchar + word[index+1:]

def insert(word):
    #97-122 are a-z in ascii
    r = random.randint(97,122)
    newchar = chr(r)
    index = random.randint(0,len(word))
    return word[:index] + newchar + word[index:]

def delete(word):
    index = random.randint(0,len(word)-1)
    return word[:index] + word[index+1:]

def scramble(word, maxdistance):
    dist = random.randint(1,maxdistance)
    for i in range(dist):
        operation = random.randint(0,2)
        if (operation == 0):
            word = replace(word)
        elif (operation == 1):
            word = insert(word)
        elif (operation == 2):
            word = delete(word)

    return word

fileIn = open(filename,'r')
fileOut = open(filename.split(".")[0]+"_edited_" + str(maxeditdistance) + ".txt",'w')

for line in fileIn:
    fileOut.write(scramble(line[:-1],maxeditdistance)+"->"+line)

import sys

if (len(sys.argv) < 3):
    print "Usage:\npython removewords.py <wordlist.txt> <treshold>"
    sys.exit(0)

filename = sys.argv[1]
treshold = int(sys.argv[2])

f = open(filename,'r')
new_filename = filename.split(".")[0] + "_pruned_at_" + str(treshold) + ".txt"
fout = open(new_filename,'w')
for line in f:
    if(int(line.split(" = ")[1]) > treshold):
        fout.write(line);
print "Created file " + new_filename

#!/bin/bash

usage="$(basename "$0") [-h] [-s FILE] -- Parse all xml/html recursively and extract all text to FILE

where:
    -h  show this help text
    -s  set the output FILE"

function start {
  for f in $(find ./ -name '*.xml' -o -name '*.html');
  do sed -e 's/<[^>]*>//g' $f >> ${OUTPUT}
  done
}


OUTPUT=./results.txt;
while getopts ':hs:' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    s) OUTPUT=$OPTARG
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

start

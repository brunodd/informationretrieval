%!TEX root = report.tex

\subsection{wordlist}
First we parsed the whole BNC counting the occurrence of each word and we saved this list of words with their frequencies. We assumed that, because the BNC is a collection of all sorts of written and spoken text, it contains incorrect words. We created a script to prune all the words with a frequency below a certain threshold. Our motivation for this is to remove all the words that are accidentally written wrong. Later in the tests will we try different thresholds to find an optimal solution.

\subsection{Edit Distance}
We use the Levenshtein distance as edit distance. i.e. any of the three operations: insert, delete, replace will increase the edit distance with one [\cite{levenshtein}]. We tried three different approaches to find words within a certain Levenshtein distance.
\subsubsection{Naive approach}
At first we used the approach used by \cite{norvig}. This approach will take a word and find \emph{every} string within a given edit distance. The following algorithm describes how we find all the words within edit distance 1 of word \emph{w}. To find words with a distance 2, just repeat the function on the outcome of the first time, etc...
\begin{algorithm}
\caption{Find all words within edit distance one}
\begin{algorithmic}[1]
\State Create empty Set S
\For {every index i in \emph{w}} S $\leftarrow$ \emph{w}.delete(i) \EndFor
\For {every index i in \emph{w}}  
\For {every character c in [a-z]} S $\leftarrow$ \emph{w}.insert(i,c) \EndFor
\EndFor
\For {every index i in \emph{w}}
\For {every character c in [a-z]}  S $\leftarrow$ \emph{w}.replace(i,c) \EndFor
\EndFor \\
\Return S
\end{algorithmic}
\end{algorithm}

\subsubsection{Edit Distance calculation}
For another approach we calculate the edit distance between two words in stead
of finding all words within an edit distance. To calculate the edit distance we
used the Wagner-Fischer algorithm [\cite{wag-fisch}]. The Levenshtein distance
between strings \emph{w} and \emph{v} is then calculated as described below.
\begin{algorithm}
\caption{Wagner-Fischer algorithm}
\begin{algorithmic}[1]
\State let $l_1$ = w.length and $l_2$ = v.length
\State create matrix = int[$l_1$ + 1][$l_2$ + 1]
\For {i in [0..$l_1$]} matrix[i][0] = i \EndFor
\For {i in [0..$l_2$]} matrix[0][i] = i \EndFor
\For {i in [0..$l_1$-1]}
	\For {j in [0..$l_2$-1]}
		\If {w[i] == v[j]}  
			\State matrix[i+1][j+1] = matrix[i][j] 
		\Else 
			 \State matrix[i+1][j+1]= min(matrix[i][j + 1] + 1,
			 \Comment{deletion}
			 \State matrix[i + 1][j] + 1,
			 \Comment{insertion}
			 \State matrix[i][j] + 1)
			 \Comment{substitution}
		\EndIf
	\EndFor
\EndFor \\
\Return matrix[$l_1$][$l_2$]
\end{algorithmic}
\end{algorithm}
The biggest advantage of this approach is that, instead of incrementing the distance with one on an insert, delete and replace, we could use different weights for each. We could even use a substitution matrix that represents the chance that a character is replaced by another character. 
\newpage
\subsubsection{Levenshtein Automaton}
For our third approach we used a Levenshtein automaton.
A Levenshtein automaton for a string \emph{w} and a number \emph{n} is a finite state automaton (FSA) that can recognize the set of all strings whose Levenshtein distance from \emph{w} is at most \emph{n}. \\
So given a string and an edit distance, we create an $\epsilon-NFA$ that accepts all strings within this edit distance. As we didn't immediately find a library that suited our needs, we implemented the code for constructing the FSA ourselves. Then we implemented an existing algorithm to convert the $\epsilon-NFA$ to a DFA \citep{automata}. For visualizing the FSA we used the Graphviz visualization software. \\
e.g. the $\epsilon-NFA$ for the word "longword" with edit distance 3 looks as follows, where 0,0 is the start state.\\
\includegraphics[scale=0.25]{resources/test_longword.png}
\newpage
All words in this example that that end up in state 8,0 have edit distance 0 (so only the original word). Words that end up in 8,1 have edit distance 1 etc,...
\\\\
When we have this $\epsilon-NFA$, we convert it to a DFA, which is exponentially larger, but extremely more efficient for matching strings because it is deterministic. We will not display the DFA for the previous example as it is visually incomprehensible.

\subsection{Isolated term correction}
An isolated term corrector will try to correct each word individually without taking the context into account.
First we check if a word is correct by going through our lexicon. If this word occurs in the lexicon, we assume it is a correct word. If it does not appear we assume it is spelled incorrectly. In the latter case we try to find an optimal correction.
\\
For this we use a part of the approach proposed by \cite{norvig}. He makes use of the Bayes rule, which states: 
\begin{equation}
 P(c|w) = \frac{P(w|c)*P(c)}{P(w)}
\end{equation}
The chance that a word \emph{c} was meant when the word \emph{w} was typed equals the chance that a word \emph{w} is typed when the word \emph{c} was meant times the chance for word \emph{c} divided by the chance for word \emph{w}.
Because $P(w)$ is a constant, given the word \emph{w}, we can leave this out which leaves us with
\begin{equation}
 P(c|w) \propto P(w|c)*P(c)
\end{equation}
 Now we want to find word \emph{c} (the correct word) that has the highest probability of being the correction of \emph{w}.\\
$P(c)$ is just the frequency of the word, which is stored in our lexicon. 
For now we use a simplistic value for $P(w|c)$. We just say that \emph{$w_1$} with a Levenshtein distance one from \emph{c} has an infinitely higher chance of being typed when \emph{c} was meant, than \emph{$w_2$} with an edit distance of two from \emph{c}. In our algorithm this means if no word of edit distance one was found, we try to find a word of edit distance two.\\
Our generic algorithm works as follows:
\begin{algorithm}
\caption{Algorithm for correcting word \emph{w}}
\begin{algorithmic}[1]
	\For {editDistance in [1..5]}
		\State create new set of candidates S
		\For {all words \emph{v} in lexicon} 
			\If {\emph{v} within editDistance of \emph{w}}
				\State S $\leftarrow$ \emph{v}
			\EndIf
		\EndFor
		\If {S not empty} \Return element in S with highest frequency \EndIf
	\EndFor \\
	\Return \emph{w} \Comment{No correction found}
\end{algorithmic}
\end{algorithm}
\newpage
Note that we do not specify which algorithm we use for the edit distance. Later we will discuss how we test the different algorithms and what their advantages and disadvantages are.

\subsection{Context sensitive correction}
Our context sensitive correction is based on tri-gram correction.
First we need a list of tri-grams and their frequencies in the English language. Our list of tri-grams has 2 origins. The first (as mentioned in \autoref{sec:resources}) is the Corpus of Contemporary American English, the
second is a self made file generated from parsing the BNC files.

\subsubsection{Tri-gram correction}
The tri-gram correction is based on an approach proposed in \cite{trigram-probabilities}. 
The following algorithm describes how we try to correct a tri-gram.
\begin{algorithm}
\caption{Tri-gram correction for context sensitive spell correction}
\begin{algorithmic}[1]
\Require A tri-gram \emph{t} consisting of 3 consecutive words
\Ensure A correct tri-gram

\If {\emph{t} in lexicon} \Return \emph{t} \EndIf
\State create new correction set S
\For {i in [1..3]}
	\Comment{consider edit distance 1 to 3}
		\For {every word \emph{w} within edit distance i of \emph{t}[0]} S $\leftarrow$ \emph{t}.replace(\emph{t}[0], \emph{w}) \EndFor
		\For {every word \emph{w} within edit distance i of \emph{t}[1]} S $\leftarrow$ \emph{t}.replace(\emph{t}[1], \emph{w}) \EndFor
		\For {every word \emph{w} within edit distance i of \emph{t}[2]} S $\leftarrow$ \emph{t}.replace(\emph{t}[2], \emph{w}) \EndFor
	
	\If {S not empty} \Return element in S with highest frequency \EndIf
\EndFor
\end{algorithmic}
\end{algorithm}

\subsubsection{Context sensitive text correction}
If we want to analyse an entire sentence and propose the best context sensitive correction, we first have to divide the sentence into tri-grams. We transform the sentence to lowercase, get rid of punctuation and just loop over all but the last two words and concatenate them with the next two consecutive words to form a collection of tri-grams. For a sentence with \emph{n} words, we get \emph{n}-2 trigrams. e.g the sentence \emph{The bird flew so high in the sky.} will result in the following list of tri-grams: \{\emph{the bird flew}, \emph{bird flew so}, \emph{flew so high}, \emph{so high in}, \emph{high in the}, \emph{in the sky}\}. \\
This first part also matches the approach of \cite{trigram-probabilities}. Wat you get is a suggestion (or no suggestion when none is found) for every tri-gram in the sentence. We then wanted to find a way to propose one sentence as a best solution for the original sentence, taking into account all of the suggested tri-grams. Unfortunately we didn't find any previous research on this. As a solution we designed our own (rather simple) algorithm to combine all the proposed solutions.
\\
First we give a score \emph{S} to each proposed tri-gram: $S = \frac{frequency}{X^{i}}$. Where i is the edit distance and we can vary X to fine-tune our results (We take X=5 for now). 

% for i in numberofwords
% for all proposed words for position i, add all scores of trigrams that propose that word
% replace word at position i by word with the highest score
\begin{algorithm}
\begin{algorithmic}[1]
\For {i in numberOfWords} 
	\For {proposed words w for position i} add scores of tri-grams that propose w \EndFor
	\State replace word at position i by word with the highest accumulated score.
\EndFor
\end{algorithmic}
\end{algorithm}
